import { STATUS_LIKE, SUBMIT_COMMENT } from "../constant/CommentConstant";

const initialState = {
  dataComment: [
    {
      id: 1,
      name: "User 1",
      content: " Tuyệt vời, siu phẩm luôn á  !!!",
      hinhAnh:
        "https://scontent.fsgn2-7.fna.fbcdn.net/v/t39.30808-1/278855044_983320765653187_134320280336234612_n.jpg?stp=dst-jpg_p480x480&_nc_cat=100&ccb=1-7&_nc_sid=7206a8&_nc_ohc=5wlsFPqY6CMAX-FBGk2&_nc_ht=scontent.fsgn2-7.fna&oh=00_AfAIlsl9MqIugDnwHrV_zkqin8rvjuet6cMdEmIwsW6mZA&oe=638314C1",
      statusLike: false,
      numberLike: 69,
    },
    {
      id: 2,
      name: "User 2",
      content: "Phim này cũng bình thường",
      hinhAnh:
        "https://scontent.fsgn2-6.fna.fbcdn.net/v/t39.30808-6/298525342_1808339819510593_3395264989019877620_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=VPr2f9IEPlYAX-8boC8&tn=a-4yN6nFzcF5Of59&_nc_ht=scontent.fsgn2-6.fna&oh=00_AfBGIg8tv3i7C6DrNPi1VlCdQG8agR8VIdDoRuOl8JbRgQ&oe=638383B4",
      statusLike: false,
      numberLike: 19,
    },
  ],
};

export const CommentReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case STATUS_LIKE:
      {
        let cloneDataComment = [...state.dataComment];
        let index = cloneDataComment.findIndex(
          (dataComment) => dataComment.id == payload
        );
        if (index != -1) {
          cloneDataComment[index].statusLike =
            !cloneDataComment[index].statusLike;
        }
        if (cloneDataComment[index].statusLike) {
          cloneDataComment[index].numberLike++;
        } else {
          cloneDataComment[index].numberLike--;
        }
        return { ...state, dataComment: cloneDataComment };
      }
      break;
    case SUBMIT_COMMENT: {
      state.dataComment.unshift(payload);
      return { ...state };
    }
    default:
      return state;
  }
};
